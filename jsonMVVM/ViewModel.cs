﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace jsonMVVM
{
    class ViewModel : INotifyPropertyChanged
    {

        private string json;
        private string meme;
        private string res;
        private string res1;
        private string res2;

        public string Json
        {
            get { return json; }
            set
            {
                json = value;
                OnPropertyChanged("Json");
            }
        }
        public string Meme
        {
            get { return meme; }
            set
            {
                meme = value;
                OnPropertyChanged("Meme");
            }
        }
        public string Res
        {
            get { return res; }
            set
            {
                res = value;
                OnPropertyChanged("Res");
            }
        }
        public string Res1
        {
            get { return res1; }
            set
            {
                res1 = value;
                OnPropertyChanged("Res1");
            }
        }
        public string Res2
        {
            get { return res2; }
            set
            {
                res2 = value;
                OnPropertyChanged("Res2");
            }
        }

        private ObservableCollection<Model> lecturers = new ObservableCollection<Model>();
        public ObservableCollection<Model> Lecturers
        {
            get { return lecturers; }
            set { lecturers = value; }
        }

        public ViewModel()
        {
            Json = @"{

  'Email': 'james@exampleee.com',
  'Active': 'jaja',
  'Work': 'plus',
}";
            Model Meme = JsonConvert.DeserializeObject<Model>(Json);
            Res = Meme.Email;
            Res1 = Meme.Active;
            Res2 = Meme.Work;

            var tmp = GetAllLecturers();
            foreach (var item in tmp)
            {
                Lecturers.Add(item);
            }
        }     

        private ObservableCollection<Model> GetAllLecturers()
        {
            ObservableCollection<Model> result = new ObservableCollection<Model>();
                result.Add(
                    new Model
                    {
                        Email = Res,
                        Active = Res1,
                        Work = Res2
                    });          
            return result;
        }

        public void OnPropertyChanged([CallerMemberName]string prop = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        public event PropertyChangedEventHandler PropertyChanged;
    }
}

